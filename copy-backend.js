let copy = require('copy');

function assertError(err, errorMessage, done) {
  if (err) {
    console.log(errorMessage);
    process.exit(1);
  }

  done();
}

function copyBlob(what, where, errorMessage, done) {
  copy(what, where, function(err) {
    assertError(err, errorMessage, done);
  })
}

function copyFile(what, where, errorMessage, done) {
  copy.one(what, where, {flatten: true}, function(err) {
    assertError(err, errorMessage, done);
  });
}

copyBlob('src/backend/*.php', 'build/backend', 'Error when copying php files', function() {
  copyFile('src/backend/sourceWebs.json', 'build/backend', 'Error when copying sourceWebs.json', function() {
    copyFile('src/backend/.htaccess', 'build/', 'Error when copying .htaccess', function() {
      console.log('Backend copied');
      process.exit(0);
    })
  })
});

