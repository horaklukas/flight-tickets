// @flow

import * as rss from './utils/rss';
import * as request from './utils/dataRequests';

import type {Channel, DispatchFnc, Action} from './types';

export const fetchOffers = (dispatch: DispatchFnc, channel: Channel) => {
  dispatch({type: 'CHANNEL_LOADING', payload: channel.id});

  request.fetchOffers(channel.id)
    .then((offers) => {
      let payload = {
        id: channel.id,
        offers: rss.parseOffers(offers)
      };
      dispatch({type: 'CHANNEL_LOAD_SUCCEDED', payload: payload});
    })
    .catch((e) => {
      dispatch({type: 'CHANNEL_LOAD_FAILED', payload: channel.id});
      console.warn('Offers fetch error for', channel.id, ',', e);
    });
};

export const fetchChannels = (dispatch: DispatchFnc) => {
  request.fetchChannels()
    .then((channels: Channel[]) => {
      dispatch({type: 'CHANNELS_LOADED', payload: channels});
      channels.forEach(channel => fetchOffers(dispatch, channel));
    })
    .catch(function(e) {
      console.warn('Channels fetch error,', e);
    });
};

export const changeFilter = (value: string): Action => {
  return {type: 'FILTER_CHANGE', payload: value};
}