// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/AppContainer';

import 'bootstrap/dist/fonts/glyphicons-halflings-regular.eot';
import 'bootstrap/dist/fonts/glyphicons-halflings-regular.svg';
import 'bootstrap/dist/fonts/glyphicons-halflings-regular.ttf';
import 'bootstrap/dist/fonts/glyphicons-halflings-regular.woff';
import 'bootstrap/dist/fonts/glyphicons-halflings-regular.woff2';
import 'bootstrap/dist/css/bootstrap.css';
import './styles/index.css';

ReactDOM.render(<App />, document.getElementById('root'));
