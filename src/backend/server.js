var express = require('express');
var request = require('request');
var app     = express();

var sources = require('./sourceWebs.json');
var port = process.env.PORT || '3003';

app.get('/sources', function(req, res) {
  res.json(sources);
});

app.get('/offers/:channel', function(req, res) {
  var channelId = req.params.channel,
    channel,
    options;

  channel = sources.filter(source => source.id === channelId)[0];
  options = {
    url: channel.url,
    headers: {
      "User-Agent": "request" // prevent deny access by web antimalware protection
    }
  };

  request(options, function(error, response, xml) {
    if (error) {
      res.status(500).send("Error at request for channel '" + channel.url + "'");
    } else if (response.statusCode === 200) {
      res.send(xml);
    } else {
      res.status(response.statusCode).send("Cannot load, reason: " + response.statusMessage);
    }
  });
});

app.listen(port);
console.log('Server listen at ' + port);

exports = module.exports = app;
