<?php
  $sources = json_decode(file_get_contents(__DIR__ . '/sourceWebs.json'));
  $channelId = $_GET['channel'];
  $channel = findChannelById($channelId, $sources);

  if ($channel === FALSE) {
    responseError(404, "Channel not found");
  }

  $opts = array(
    'http'=>array(
      'method'=>"GET",
      // prevent deny access by web antimalware protection
      'header'=>"User-Agent: request\r\n"
    )
  );

  $context = stream_context_create($opts);
  $channelContent = file_get_contents($channel->url, FALSE, $context);

  if ($channelContent === FALSE) {
    responseError(400, error_get_last()['message']);
  } else {
    echo $channelContent;
  }

  function findChannelById($channelId, $channels) {
    foreach ($channels as $channel) {
      if($channel->id === $channelId) {
        return $channel;
      }
    }

    return FALSE;
  }

  function responseError($httpCode, $message) {
    http_response_code($httpCode);
    exit($message);
  }
?>
