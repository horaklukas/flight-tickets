// @flow

import React from 'react';
import Panel from '../Panel';
import TicketOffer from '../TicketOffer';
import classNames from 'classnames';
import type {Channel, OffersByChannel, Offer} from '../../types';

import styles from './style.css';

type Props = {
  channels: Channel[],
  offers: OffersByChannel,
  filter: string,
  actions: {[actionName: string]: (any) => void}
};

type OfferWithChannel = Offer & {channel: string};

const addChannelToOffer = (channelName: string, offer: Offer): OfferWithChannel => {
  return Object.assign({}, {channel: channelName}, offer);
}

export default ({channels, offers, filter, actions}: Props) => {
  const lowerCaseFilter = filter.toLowerCase();
  const classes = classNames("list-group", styles.offers);
  const allOffers = channels.filter(channel => offers[channel.id])
    .map(({id, name}) => {
      return offers[id].map(offer => addChannelToOffer(name, offer))
    })
    .reduce((all, channelOffers) =>  all.concat(channelOffers), [])
    .filter(offer => offer.title.toLowerCase().indexOf(lowerCaseFilter) > -1)
    .sort((ch1, ch2) => ch2.publishDate.getTime() - ch1.publishDate.getTime() );

  return (
    <div className="App">
      <Panel channels={channels} filter={filter} onFilterChange={actions.changeFilter} />
      <div className={classes}>
        {allOffers.map(({title, url, publishDate, channel}: OfferWithChannel, index) => (
          <TicketOffer key={`${url}${index}`} channel={channel} publishDate={publishDate} url={url} title={title} />
        ))}
      </div>
    </div>
  );
}