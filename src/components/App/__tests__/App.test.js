import React from 'react';
import {shallow, mount} from 'enzyme';
import App from '../index';
import TicketOffer from '../../TicketOffer';
import {channels as channels, offersJson as offers}
  from '../../../../test/__data__/channelsOffers';

describe('<App />', function () {
  it('render ticket offfers', function () {
    const app = shallow(
      <App channels={channels} offers={offers} filter={''} actions={{}} />
    );
    const tickets = app.find(TicketOffer);

    expect(tickets.length).toBe(8);
  });

  it('render tickets offers sorted by date', function () {
    const app = shallow(
      <App channels={channels} offers={offers} filter={''} actions={{}} />
    );
    const tickets = app.find(TicketOffer);

    expect(tickets.get(0).props.title).toBe('Praha - Bergen za 1 830 Kč');
    expect(tickets.get(1).props.title).toBe('Dublin - Las Vegas za 8 999 Kč');
    expect(tickets.get(2).props.title).toBe('NIKI: Kanáry - Fuerteventura - 1 890 Kč');
    expect(tickets.get(3).props.title).toBe('Tranzitní víza do Qataru nově zdarma');
    expect(tickets.get(4).props.title).toBe('Přímý let do Barcelony za 2061 Kč');
    expect(tickets.get(5).props.title).toBe('Miami ze Stockholmu – 6628 Kč');
    expect(tickets.get(6).props.title).toBe('Condor: Baleárské ostrovy - Ibiza - 1 650 Kč');
    expect(tickets.get(7).props.title).toBe('TUIfly: Řecké ostrovy - Kréta - 1 950 Kč');
  });

  it('render offers filtered by filter value', () => {
    const app = shallow(
      <App channels={channels} offers={offers} filter={'do'} actions={{}} />
    );
    const tickets = app.find(TicketOffer);

    expect(tickets.length).toBe(3);
    expect(tickets.get(0).props.title).toBe('Tranzitní víza do Qataru nově zdarma');
    expect(tickets.get(1).props.title).toBe('Přímý let do Barcelony za 2061 Kč');
    expect(tickets.get(2).props.title).toBe('Condor: Baleárské ostrovy - Ibiza - 1 650 Kč');
  });

  it('use search case insensitive', () => {
    const app = shallow(
      <App channels={channels} offers={offers} filter={'ba'} actions={{}} />
    );
    const tickets = app.find(TicketOffer);

    expect(tickets.length).toBe(2);
    expect(tickets.get(0).props.title).toBe('Přímý let do Barcelony za 2061 Kč');
    expect(tickets.get(1).props.title).toBe('Condor: Baleárské ostrovy - Ibiza - 1 650 Kč');
  });

  it('use filter value case insensitive', () => {
    const app = shallow(
      <App channels={channels} offers={offers} filter={'Ba'} actions={{}} />
    );
    const tickets = app.find(TicketOffer);

    expect(tickets.length).toBe(2);
    expect(tickets.get(0).props.title).toBe('Přímý let do Barcelony za 2061 Kč');
    expect(tickets.get(1).props.title).toBe('Condor: Baleárské ostrovy - Ibiza - 1 650 Kč');
  });
});
