// @flow

import React from 'react';
import classNames from 'classnames';
import styles from './style.css';

type Props = {
  title: string,
  url: string,
  publishDate: Date,
  channel: string
};

function formatDate(date) {
  let year = date.getFullYear(),
    month = date.getMonth() + 1,
    day = date.getDate();

  return `${day}.${month}.${year}`;
}

export default ({title, url, publishDate, channel}: Props) =>{
    let date = formatDate(publishDate);

    return (
      <a href={url} title={url} className="list-group-item" target="blank">
        <span className={styles.date}>{date}</span>
        <span className={styles.title}>{title}</span>
        <span className={classNames('pull-right', styles.vendor)}>
          {channel}
        </span>
      </a>
    );
};
