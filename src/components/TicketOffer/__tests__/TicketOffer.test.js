import React from 'react';
import {shallow, mount} from 'enzyme';
import TicketOffer from '../index';

describe('<TicketOffer />', () => {
  it('should render without crashing', () => {
    const offer = mount(<TicketOffer title="" url="" publishDate={new Date} />);
  });

  it('should format date', () => {
    const date = new Date(2007, 6, 23);
    const offer = shallow(<TicketOffer title="" url="" publishDate={date} />);

    expect(offer.find('span').first().text()).toEqual('23.7.2007');
  });
});
