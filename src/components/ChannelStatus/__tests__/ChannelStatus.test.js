import React from 'react';
import {Glyphicon} from 'react-bootstrap';
import {Columns as Loader} from 'loin';
import {shallow, mount} from 'enzyme';
import ChannelStatus from '../index';

describe('<ChannelStatus />', () => {
  it('render success icon for loaded channel', () => {
    const status = shallow(<ChannelStatus id="" name="" url="" status="LOAD_SUCCEDED"/>);

    expect(status.find(Glyphicon).prop('glyph')).toBe('ok');
  });

  it('render loader for currently loaded channel', () => {
    const status = shallow(<ChannelStatus id="" name="" url="" status="LOADING"/>);

    expect(status.find(Loader).length).toBe(1);
  });

  it('render error icon for failed loaded channel', () => {
    const status = shallow(<ChannelStatus id="" name="" url="" status="LOAD_FAILED"/>);

    expect(status.find(Glyphicon).prop('glyph')).toBe('remove');
  });
});
