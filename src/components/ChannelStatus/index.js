// @flow

import React from 'react';
import {Glyphicon} from 'react-bootstrap';
import classNames from 'classnames';
import {Columns as Loader} from 'loin';
import type {Channel, ChannelStatus} from'../../types';

import styles from './style.css';

type Props = Channel;

const getIconByStatus = (status: ChannelStatus) => {
  switch (status) {
    case 'LOADING':
      return <Loader color={'#337ab7'} count={5} />;
    case 'LOAD_SUCCEDED':
      return <Glyphicon glyph="ok" className="text-success" title="Loaded" />;
    case 'LOAD_FAILED':
      return <Glyphicon glyph="remove" className="text-danger" title="Load failed" />;
    default:
      return null;
  }
};

export default ({id, name, status}: Props) => {
  const classes = classNames('list-group-item', styles.status);

  return (
    <div className={classes} key={id}>
      {name}
      <div className="pull-right">{getIconByStatus(status)}</div>
    </div>
  )
}
