// @flow

import React, {Component} from 'react';
import {Panel} from 'react-bootstrap';
import Header from './Header';
import ChannelStatus from '../ChannelStatus';
import type {Channel} from'../../types';

import styles from './style.css';

type Props = {
  channels: Channel[]
};

const isLoadFailed = (channel) => channel.status === 'LOAD_FAILED';

const countLoadedChannels = (count, channel) =>  {
  return channel.status === 'LOADING' ? count : count + 1;
};

export default class ChannelsList extends Component {
  props: Props;

  state: {
    open: boolean
  };

  constructor(props: Props) {
      super(props);

      this.state = {
        open: false
      };
    }

    toggle() {
      this.setState(prevState => {
        return {open: !prevState.open};
      })
    }

    render() {
      const {channels} = this.props;
      const {open} = this.state;
      const count = channels.length;
      const loaded = channels.reduce(countLoadedChannels, 0);
      const header = <Header channels={{count, loaded}} open={open} onClick={() => this.toggle()} />;

      const bsStyle = loaded === count && count !== 0
        ? (channels.some(isLoadFailed) ? 'danger' : 'success')
        : 'default';

      return (
        <Panel header={header} className={styles.channelsList} collapsible={true}
          expanded={open} bsStyle={bsStyle}>
          {
            channels.map(channel => <ChannelStatus key={channel.id} {...channel} />)
          }
        </Panel>
      );
    }
}


