import React from 'react';
import {Panel} from 'react-bootstrap';
import {shallow} from 'enzyme';
import ChannelsList from '../index';
import Header from '../Header';
import ChannelStatus from '../../ChannelStatus';
import {channels as channelsData} from '../../../../test/__data__/channelsOffers';

describe('<ChannelsList />', () => {
  it('should display with count of channels', () => {
    const panel = shallow(<ChannelsList channels={channelsData} />);
    const header = panel.find(Panel).prop('header');

    expect(header.props.channels.loaded).toEqual(2);
    expect(header.props.channels.count).toEqual(3);
  });

  it('should render status for each channel', () => {
    const panel = shallow(<ChannelsList channels={channelsData} />);
    const statuses = panel.find(ChannelStatus);

    expect(statuses.get(0).props).toEqual(channelsData[0]);
    expect(statuses.get(1).props).toEqual(channelsData[1]);
    expect(statuses.get(2).props).toEqual(channelsData[2]);
  })
});
