import React from 'react';
import {shallow} from 'enzyme';
import Progress from '../../Progress';
import Header from '../Header';

describe('<Header />', () => {
  it('should display progress bar', () => {
    const header = shallow(<Header channels={{count: 4, loaded: 2}} />);
    const progressProps = header.find(Progress).props();

    expect(progressProps.done).toEqual(2);
    expect(progressProps.full).toEqual(4);
  });
});
