// @flow

import React from 'react';
import {Glyphicon} from 'react-bootstrap';
import Progress from '../Progress';

import styles from './style.css';

type Props = {
  channels: {
    loaded: number,
    count: number
  },
  open: boolean,
  onClick: () => void
};

export default (props: Props) => {
  const {channels, open} = props;

  return (
    <div className={styles.header} onClick={props.onClick}>
      <Glyphicon className={styles.collapse} glyph={`collapse-${open ? 'up':'down'}`} />
      <Progress label="Scanning" done={channels.loaded} full={channels.count} />
      {open ? null : <small>List of channels collapsed, click to display more info</small>}
    </div>
  )
}
