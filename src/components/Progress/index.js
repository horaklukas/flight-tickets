// @flow

import React from 'react';
import {ProgressBar} from 'react-bootstrap';
import classNames from 'classnames';
import styles from './style.css';

type Props = {
    label: string,
    done: number,
    full: number
};

export default ({done, full, label}: Props) => {
  let now = done / full || 0,
    progress = Math.round(now * 100),
    classes = classNames(
      styles.container,
      {"incomplete": now < 1}
    );

  return (
    <div className={classes}>
      <ProgressBar now={progress} label={`${label} ${progress}%`} />
    </div>
  );
};
