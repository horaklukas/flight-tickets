// @flow

import React from 'react';
import {InputGroup, FormControl, Glyphicon} from 'react-bootstrap';

import styles from './style.css';

type Props = {
    value: string,
    onChange: (value: string) => void;
};

export default ({value, onChange}: Props) => {
  return (
    <div className={styles.container}>
      <InputGroup>
        <FormControl value={value || ''} onChange={({target}) => onChange(target.value)} />
        <InputGroup.Addon className={styles.clear}>
          <Glyphicon glyph="remove" onClick={() => onChange('')} />
        </InputGroup.Addon>
      </InputGroup>
    </div>
  );
};
