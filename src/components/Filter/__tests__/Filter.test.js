import React from 'react';
import {FormControl, InputGroup, Glyphicon} from 'react-bootstrap';
import {shallow, mount} from 'enzyme';
import Filter from '../index';

describe('<Filter />', () => {
  it('should render without crashing', () => {
    const filter = mount(<Filter />);
  });

  it('display supplied value', () => {
    const offer = shallow(<Filter value="231" />);

    expect(offer.find(FormControl).prop('value')).toBe('231');
  });

  it('display display empty value if none supplied', () => {
    const offer = shallow(<Filter value={null} />);

    expect(offer.find(FormControl).prop('value')).toBe('');
  });

  it('invoke onChange callback every time input change', () => {
    const onChangeSpy = jest.fn();
    const offer = shallow(<Filter value={null} onChange={onChangeSpy} />);
    const input = offer.find(FormControl);

    input.prop('onChange')({target: {value: 'L'}});
    input.prop('onChange')({target: {value: 'Lo'}});
    input.prop('onChange')({target: {value: 'Lon'}});

    expect(onChangeSpy.mock.calls.length).toBe(3);
    expect(onChangeSpy.mock.calls[0][0]).toBe('L');
    expect(onChangeSpy.mock.calls[1][0]).toBe('Lo');
    expect(onChangeSpy.mock.calls[2][0]).toBe('Lon');
  });

  it('invoke onChange callback on Clear addon click', () => {
    const onChangeSpy = jest.fn();
    const offer = shallow(<Filter value={null} onChange={onChangeSpy} />);
    const addon = offer.find(Glyphicon);

    addon.simulate('click');

    expect(onChangeSpy.mock.calls.length).toBe(1);
    expect(onChangeSpy.mock.calls[0][0]).toBe('');
  });
});
