import React from 'react';
import {shallow} from 'enzyme';
import Panel from '../index';
import ChannelsList from '../../ChannelsList';
import Filter from '../../Filter';
import {channels as channelsData} from '../../../../test/__data__/channelsOffers';

describe('<Panel />', () => {
  it('render channelsList', () => {
    const panel = shallow(<Panel filter={''} channels={channelsData} />);
    const list = panel.find(ChannelsList);

    expect(list.prop('channels')).toEqual(channelsData);
  });

  it('render filter', () => {
    const onChangeCb = jest.fn();
    const panel = shallow(
      <Panel filter={'Bal'} onFilterChange={onChangeCb} channels={channelsData} />
    );
    const filter = panel.find(Filter);

    expect(filter.prop('value')).toBe('Bal');
    expect(filter.prop('onChange')).toBe(onChangeCb);

  });
});
