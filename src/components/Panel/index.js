// @flow

import React from 'react';
import ChannelsList from '../ChannelsList';
import Filter from '../Filter';
import type {Channel} from'../../types';

import styles from './style.css';

type Props = {
  channels: Channel[],
  filter: string,
  onFilterChange: (filter: string) => void;
};

export default ({channels, filter, onFilterChange}: Props) => {

  return (
    <div className={styles.panel}>
      <ChannelsList channels={channels} />
      <Filter value={filter} onChange={onFilterChange} />
    </div>
  )
}


