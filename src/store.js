// @flow

import type {AppState, Action, Channel, OffersByChannel, ChannelStatus} from './types';

const defaultState: AppState = {
  channels: [],
  offers: {},
  filterValue: ''
};

const setChannelStatus = (channels: Channel[], channelId: string, status: ChannelStatus) => {
  return channels.map(channel => {
      return channel.id === channelId ? {...channel, status: status} : channel;
  });
}

const reduceChannels = (channelsState: Channel[], action: Action): Channel[] => {
  switch (action.type) {
    case 'CHANNELS_LOADED':
      return action.payload;

    case 'CHANNEL_LOADING':
      return setChannelStatus(channelsState, action.payload, 'LOADING');

    case 'CHANNEL_LOAD_FAILED':
      return setChannelStatus(channelsState, action.payload, 'LOAD_FAILED');

    case 'CHANNEL_LOAD_SUCCEDED':
      return setChannelStatus(channelsState, action.payload.id, 'LOAD_SUCCEDED');

    default:
      return channelsState;
  }
};

const reduceOffers = (offersState: OffersByChannel, action: Action): OffersByChannel => {
  switch (action.type) {
    case 'CHANNEL_LOAD_SUCCEDED':
      return  {
        ...offersState,
        [action.payload.id]: action.payload.offers
      };

    default:
      return offersState;
  }
};

const reduceFilterValue = (filterValue: string, action: Action): string => {
  switch (action.type) {
    case 'FILTER_CHANGE':
      return  action.payload;

    default:
      return filterValue;
  }
};

export default (state: AppState = defaultState, action: Action): AppState => {
  return {
    channels: reduceChannels(state.channels, action),
    offers: reduceOffers(state.offers, action),
    filterValue: reduceFilterValue(state.filterValue, action)
  }
};