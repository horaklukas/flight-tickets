// @flow

import React, {Component} from 'react';
import App from '../components/App';
import appStore from '../store';
import {fetchChannels, changeFilter} from '../actions';
import type {AppState, Action} from '../types';

type Props = {};

export default class AppContainer extends Component {
  state: AppState;

  constructor(props: Props) {
    super(props);

    this.state = appStore(undefined, {type: 'INIT_STORE'});
  }

  componentDidMount() {
    fetchChannels(action => this.dispatch(action));
  }

  dispatch(action: Action) {
    this.setState((prevState) => appStore(prevState, action));
  }

  render() {
    const actions = {
      changeFilter: (value) => this.dispatch(changeFilter(value))
    }

    return (
      <App channels={this.state.channels} offers={this.state.offers}
        filter={this.state.filterValue} actions={actions}/>
    );
  }
}
