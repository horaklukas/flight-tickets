// @flow

import type {Offer} from '../types';

export function parseOffers(channel: HTMLElement): Offer[] {
  var offers = [],
    items,
    i;

  items = channel.querySelectorAll('item');

  for (i = 0; i < items.length; i++) {
    let offer = items[i];
    offers.push(parseOffer(offer, channel));
  }

  return offers;
}

function parseOffer(offer: HTMLElement): Offer {
  var title = offer.querySelector('title'),
    link = offer.querySelector('link'),
    price = offer.querySelector('price'),
    priceValue = price ? ' ' + price.innerHTML : '',
    publishDate = offer.querySelector('pubDate');

    return {
      title: title ? title.innerHTML + priceValue : '',
      url: link ? link.innerHTML : '',
      publishDate: publishDate ? new Date(Date.parse(publishDate.innerHTML)) : new Date()
    };
}
