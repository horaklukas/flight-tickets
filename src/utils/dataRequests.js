// @flow

import PromiseRequest from 'xhr-promise';
import type {ChannelId, Channel} from '../types';

const xhr = new PromiseRequest(),
  docParser = new DOMParser();

const checkSuccesfullResponse = result => {
  if (result.status !== 200) {
    throw new Error('http status: ' + result.status);
  }
  return result;
};

export function fetchChannels(): Promise<Channel[]> {
  return xhr.send({method: 'GET', url: `/sources`})
    .then(checkSuccesfullResponse)
    .then(result => result.responseText);
}

export function fetchOffers(id: ChannelId): Promise<HTMLElement> {
  return xhr.send({method: 'GET', url: `/offers/${id}`})
    .then(checkSuccesfullResponse)
    .then(({xhr}) => docParser.parseFromString(xhr.responseText, "text/xml"));
}
