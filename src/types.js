// @flow

export type ChannelId = string;
export type ChannelStatus =
	| 'LOADING'
	| 'LOAD_FAILED'
	| 'LOAD_SUCCEDED';

export type Channel = {
	id: ChannelId,
	name: string,
	url: string,
	status: ChannelStatus
};

export type Offer = {
	title: string,
	url: string,
	publishDate: Date
};

export type ChannelOffer = {
	channel: ChannelId,
	title: string,
	url: string,
	publishDate: Date
};

export type OffersByChannel = {[id: ChannelId]: Offer[]};

export type AppState = {
	channels: Channel[],
	offers: OffersByChannel,
	filterValue: string
};

export type Action =
	| {type: 'INIT_STORE' }
	| {type: 'CHANNELS_LOADED', payload: Channel[] }
	| {type: 'CHANNEL_LOADING', payload: ChannelId }
	| {type: 'CHANNEL_LOAD_FAILED', payload: ChannelId }
	| {type: 'CHANNEL_LOAD_SUCCEDED', payload: {id: ChannelId, offers: Offer[]} }
	| {type: 'FILTER_CHANGE', payload: string };

export type DispatchFnc = (action: Action) => void;

export type ActionCreator = (any) => Action;
