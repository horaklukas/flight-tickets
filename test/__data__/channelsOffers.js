export const channels = [
  {id: "akcniletenky", name: "Akční letenky", url: "http://akcniletenky.com/rss.xml", status: 'LOAD_SUCCEDED'},
  {id: "jaknaletenky", name: "Jak na letenky", url: "http://feeds.feedburner.com/JakNaLetenkycz?format=xml", status: 'LOADING'},
  {id: "zaletsi", name: "Zaleť si", url: "http://zaletsi.cz/feed", status: 'LOAD_SUCCEDED'}
];

const offer = (title, date) => {
  return {title, publishDate: new Date(Date.parse(date))};
}
export const offersJson = {
  'akcniletenky': [
    offer('NIKI: Kanáry - Fuerteventura - 1 890 Kč', '29 Sep 2016 19:30:00 +0200'),
    offer('Condor: Baleárské ostrovy - Ibiza - 1 650 Kč', '14 Sep 2016 14:00:00 +0200'),
    offer('TUIfly: Řecké ostrovy - Kréta - 1 950 Kč', '1 Aug 2016 16:30:00 +0200')
  ],
  'jaknaletenky': [
    offer("Praha - Bergen za 1 830 Kč", "Thu, 29 Sep 2016 17:50:08 GMT"),
    offer("Dublin - Las Vegas za 8 999 Kč", "Thu, 29 Sep 2016 17:47:44 GMT")
  ],
  'zaletsi': [
    offer('Tranzitní víza do Qataru nově zdarma', 'Wed, 28 Sep 2016 13:43:52 +0000'),
    offer('Přímý let do Barcelony za 2061 Kč', 'Tue, 27 Sep 2016 18:10:19 +0000'),
    offer('Miami ze Stockholmu – 6628 Kč', 'Tue, 27 Sep 2016 09:20:10 +0000')
  ]
};

const docParser = new DOMParser();
export const offersXml = {
  "akcniletenky": docParser.parseFromString(
    `<?xml version="1.0" encoding="utf-8"?>
      <rss version="2.0">
      <channel>
        <title>AKČNÍ LETENKY.com</title>
        <link>http://www.akcniletenky.com</link>
      <description>Tipy na nejlevnější letenky do celého světa.</description>
      <language>cs</language>
      <item>
        <title>NIKI: Kanáry - Fuerteventura - 1 890 Kč</title>
        <link>http://www.akcniletenky.com</link>
        <pubDate>29 Sep 2016 19:30:00 +0200</pubDate>
      </item>
      <item>
        <title>Condor: Baleárské ostrovy - Ibiza - 1 650 Kč</title>
        <link>http://www.akcniletenky.com</link>
        <pubDate>14 Sep 2016 14:00:00 +0200</pubDate>
      </item>
      <item>
        <title>TUIfly: Řecké ostrovy - Kréta - 1 950 Kč</title>
        <link>http://www.akcniletenky.com</link>
        <pubDate>1 Aug 2016 16:30:00 +0200</pubDate>
      </item>
      </channel>
    </rss>`,
  "text/xml"),

  "jaknaletenky": docParser.parseFromString(
    `<?xml version="1.0" encoding="UTF-8"?>
      <?xml-stylesheet type="text/xsl" media="screen" href="/~d/styles/rss2czechfull.xsl"?><?xml-stylesheet type="text/css" media="screen" href="http://feeds.feedburner.com/~d/styles/itemcontent.css"?><rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" version="2.0">
      <channel>
        <title>JAKnaLetenky.cz</title>
        <link>http://jaknaletenky.cz</link>
        <description>JAKnaLetenky.cz - tipy na nejlevnější letenky.</description>
        <pubDate>Wed, 08 Sep 2010 10:00:00 +0200</pubDate>
        <atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="self" type="application/rss+xml" href="http://feeds.feedburner.com/JakNaLetenkycz" /><feedburner:info xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" uri="jaknaletenkycz" /><atom10:link xmlns:atom10="http://www.w3.org/2005/Atom" rel="hub" href="http://pubsubhubbub.appspot.com/" /><feedburner:emailServiceId xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0">JakNaLetenkycz</feedburner:emailServiceId><feedburner:feedburnerHostname xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0">https://feedburner.google.com</feedburner:feedburnerHostname><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="https://add.my.yahoo.com/rss?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://us.i1.yimg.com/us.yimg.com/i/us/my/addtomyyahoo4.gif">Subscribe with My Yahoo!</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.newsgator.com/images/ngsub1.gif">Subscribe with NewsGator</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://feeds.my.aol.com/add.jsp?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://o.aolcdn.com/favorites.my.aol.com/webmaster/ffclient/webroot/locale/en-US/images/myAOLButtonSmall.gif">Subscribe with My AOL</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.bloglines.com/sub/http://feeds.feedburner.com/JakNaLetenkycz" src="http://www.bloglines.com/images/sub_modern11.gif">Subscribe with Bloglines</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.netvibes.com/subscribe.php?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="//www.netvibes.com/img/add2netvibes.gif">Subscribe with Netvibes</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://fusion.google.com/add?feedurl=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://buttons.googlesyndication.com/fusion/add.gif">Subscribe with Google</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.pageflakes.com/subscribe.aspx?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.pageflakes.com/ImageFile.ashx?instanceId=Static_4&amp;fileName=ATP_blu_91x17.gif">Subscribe with Pageflakes</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.plusmo.com/add?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://plusmo.com/res/graphics/fbplusmo.gif">Subscribe with Plusmo</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.thefreedictionary.com/_/hp/AddRSS.aspx?http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://img.tfd.com/hp/addToTheFreeDictionary.gif">Subscribe with The Free Dictionary</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.bitty.com/manual/?contenttype=rssfeed&amp;contentvalue=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.bitty.com/img/bittychicklet_91x17.gif">Subscribe with Bitty Browser</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.live.com/?add=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://tkfiles.storage.msn.com/x1piYkpqHC_35nIp1gLE68-wvzLZO8iXl_JMledmJQXP-XTBOLfmQv4zhj4MhcWEJh_GtoBIiAl1Mjh-ndp9k47If7hTaFno0mxW9_i3p_5qQw">Subscribe with Live.com</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://mix.excite.eu/add?feedurl=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://image.excite.co.uk/mix/addtomix.gif">Subscribe with Excite MIX</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.webwag.com/wwgthis.php?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.webwag.com/images/wwgthis.gif">Subscribe with Webwag</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.podcastready.com/oneclick_bookmark.php?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.podcastready.com/images/podcastready_button.gif">Subscribe with Podcast Ready</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.wikio.com/subscribe?url=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.wikio.com/shared/img/add2wikio.gif">Subscribe with Wikio</feedburner:feedFlare><feedburner:feedFlare xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0" href="http://www.dailyrotation.com/index.php?feed=http%3A%2F%2Ffeeds.feedburner.com%2FJakNaLetenkycz" src="http://www.dailyrotation.com/rss-dr2.gif">Subscribe with Daily Rotation</feedburner:feedFlare>
        <item>
          <title>Praha - Bergen za 1 830 Kč</title>
          <link>http://www.jaknaletenky.cz/forum/topic60-15.html#p6689</link>
          <pubDate>Thu, 29 Sep 2016 17:50:08 GMT</pubDate>
          <description>Akční letenky - fórum.</description>
          <source url="http://www.jaknaletenky.cz/forum/gymrss.php?forum=1&amp;digest">Akční letenky - fórum.</source>
          <guid isPermaLink="true">http://www.jaknaletenky.cz/forum/topic60-15.html#p6689</guid>
        </item>
        <item>
          <title>Dublin - Las Vegas za 8 999 Kč</title>
          <link>http://www.jaknaletenky.cz/forum/topic12-195.html#p6688</link>
          <pubDate>Thu, 29 Sep 2016 17:47:44 GMT</pubDate>
          <description>Akční letenky - fórum.</description>
          <source url="http://www.jaknaletenky.cz/forum/gymrss.php?forum=1&amp;digest">Akční letenky - fórum.</source>
          <guid isPermaLink="true">http://www.jaknaletenky.cz/forum/topic12-195.html#p6688</guid>
        </item>
      </channel>
      </rss>`,
  "text/xml"),

  "zaletsi": docParser.parseFromString(
    `<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
        >

      <channel>
        <title>Zaletsi.cz</title>
        <atom:link href="http://zaletsi.cz/feed" rel="self" type="application/rss+xml" />
        <link>http://zaletsi.cz</link>
        <description></description>
        <lastBuildDate>Thu, 29 Sep 2016 13:14:50 +0000</lastBuildDate>
        <language>cs-CZ</language>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <generator>https://wordpress.org/?v=4.4.5</generator>
          <item>
          <title>Tranzitní víza do Qataru nově zdarma</title>
          <link>http://zaletsi.cz/tranzitni-viza-do-qataru-nove-zdarma</link>
          <comments>http://zaletsi.cz/tranzitni-viza-do-qataru-nove-zdarma#respond</comments>
          <pubDate>Wed, 28 Sep 2016 13:43:52 +0000</pubDate>
          <dc:creator><![CDATA[Jirka Spolek - www.zaletsi.cz]]></dc:creator>
              <category><![CDATA[Do světa]]></category>

          <guid isPermaLink="false">http://zaletsi.cz/?p=15270</guid>
          <description><![CDATA[Opět dobrá zpráva před zimou, kdy řada cestovatelů bude přesedat na katarském letišti Dohá na&#8230; Číst více]]></description>
              <content:encoded><![CDATA[<div style="padding: 15px; margin: 10px;">
      <p>Opět dobrá zpráva před zimou, kdy řada cestovatelů bude přesedat na katarském letišti Dohá na další lety směrem do jihovýchodní Asie. Katarské ministerstvo vnitro rozhodlo, že všichni pasažéři, kteří budou tranzitovat <strong>déle než 5 hodin na letišti v Dohá,</strong> mohou získat tranzitní 4denní tranzitní vízum zdarma.</p>
      <p>Můžeme se tak dostat nově do destinace, kam je za běžných okolností zatím získání turistického víza poměrně obtížná záležitost.</p>
      </div>
      <div class="text_content">
      <ul>
      <li style="margin: 15px; text-align: justify;"><strong>Co je potřeba pro získání tranzitiního víza:</strong> navazující letenka do třetí země</li>
      <li style="margin: 15px; text-align: justify;"><strong>Platnost tranzitního víza</strong>: max 4 dny</li>
      <li style="margin: 15px; text-align: justify;"><strong>Od kdy to platí:</strong> od 27. září 2016</li>
      <li style="margin: 15px; text-align: justify;"><strong>Zdroj:</strong> <strong><u><a href="http://www.qna.org.qa/en-us/News/16092619200067/New-Transit-Rules-Make-Visiting-Qatar-Easier">Qatarské ministerstvo vnitra</a></u></strong></li>
      </ul>
      </div>
      <div class="text_content">
      <h2 style="margin: 15px; text-align: justify;">Co můžete vidět v Dohá?</h2>
      <p>Tranzit v Dohá bývá obvykle do 24h, pokud si vyloženě nedáte do letenky stopover, vybrali jsme pro vás pár věcí, které byste neměli v Dohá minout.</p>
      <h3 style="margin: 15px; text-align: justify;">Souk Waqif</h3>
      <p style="margin: 15px; text-align: justify;">Arabský svět by nebyl arabským světem bez svého vlastního tržiště. Tím největším a nejznámějším je právě Souk Waqif. Doporučujeme dorazit až odpoledne okolo 16h, kdy to na tržišti začíná žít. Živo je tu každý den od soboty do čtvrtka 9-13 a 16-21, v pátek potom 16-22.</p>
      <h3 style="margin: 15px; text-align: justify;">Promenáda Corniche</h3>
      <p style="margin: 15px; text-align: justify;">Podobnou promenádu mají například v emirátském Abú Dhabí, kochat se výhledem na mrakodrapy procházkou podél palem můžete právě tady.</p>
      <h3 style="margin: 15px; text-align: justify;">Museum islámského umění</h3>
      <p style="margin: 15px; text-align: justify;">Kousek od přístavu Dohá Port vás čeká Museum islámského umění. Kromě klasických vitrínek se zajímavými exponáty (dýky, cennosti) jde i o velmi zajímavou stavbu.</p>
      <h3 style="margin: 15px; text-align: justify;">Za nákupy</h3>
      <p style="margin: 15px; text-align: justify;">Asi každý zná Dubai Mall, v Dohá mají zase <strong>City Centre Mall. </strong>Obrovské nákupní středisko, kde si můžete koupit úplně všechno, i zlaté cihly v automatu tu samozřejmě mají. Pokud si nic kupovat nebudete, nevadí, venku vás uvítá obrovský nákupní vozík, uvnitř je pak mimo jiné i zimní stadion, kde si můžete zabruslit .</p>
      <p style="margin: 15px; text-align: justify;">Kromě City Centre Mall můžete také mrknout do <strong>Villagio Shopping mall,</strong> nákupní centrum, ve kterém jezdí italské gondoly.</p>
      <h3 style="margin: 15px; text-align: justify;"><strong>A doprava?</strong></h3>
      <p style="margin: 15px; text-align: justify;">Údajně fungují 2 linky autobusů (jestli někdo ověří, určitě budeme rádi za info), nicméně na to bychom nespoléhali a vyřešili to klasickým taxi nebo si vzali Uber.</p>
      <h3 style="margin: 15px; text-align: justify;">Ubytování v Dohá</h3>
      <p style="margin: 15px; text-align: justify;">V Dohá bychom bydleli <strong><span style="text-decoration: underline;"><a href="http://www.booking.com/hotel/qa/rawda-al-khail.cs.html?aid=344738;sid=14e9fa50302baf7fa891cff5e9bb1644;checkin=2016-10-13;checkout=2016-10-14;ucfs=1;highlighted_blocks=40123001_89086668_0_2_0;all_sr_blocks=40123001_89086668_0_2_0;room1=A,A;dest_type=city;dest_id=-785169;srfid=3dce59e2e5a48d35fc418a481b0a4062f0869ea2X2;highlight_room=" target="_blank">v tomto hotelu,</a></span></strong> přibližně 15 km od letiště.</p>
      <p style="margin: 15px; text-align: justify;">
      </div>
      ]]></content:encoded>
            <wfw:commentRss>http://zaletsi.cz/tranzitni-viza-do-qataru-nove-zdarma/feed</wfw:commentRss>
          <slash:comments>0</slash:comments>
          </item>
          <item>
          <title>Přímý let do Barcelony za 2061 Kč</title>
          <link>http://zaletsi.cz/primy-let-do-barcelony-za-2061-kc</link>
          <comments>http://zaletsi.cz/primy-let-do-barcelony-za-2061-kc#respond</comments>
          <pubDate>Tue, 27 Sep 2016 18:10:19 +0000</pubDate>
          <dc:creator><![CDATA[Jirka Spolek - www.zaletsi.cz]]></dc:creator>
              <category><![CDATA[Do 3 000 Kč]]></category>
          <category><![CDATA[Levné letenky]]></category>
          <category><![CDATA[Levné ubytování]]></category>
          <category><![CDATA[O letních prázdninách]]></category>
          <category><![CDATA[Po Evropě]]></category>

          <guid isPermaLink="false">http://zaletsi.cz/?p=15267</guid>
          <description><![CDATA[Po skvělé nabídce letenek z Prahy se Barcelony se nám s podobnou akcí vytasil také&#8230; Číst více]]></description>
              <content:encoded><![CDATA[<div style="padding: 15px;margin: 10px;">
      <p>Po skvělé nabídce letenek z Prahy se Barcelony se nám s podobnou akcí vytasil také Vueling, který prodává přímý let z Prahy do katalánské metropole už za 2061 Kč. Tato cena za přímý let je nejnižší za poslední rok.&nbsp;</p>
      <p>Za velmi podobnou cenu navíc můžete do Barcelony vyrazit také z Vídně a Norimberku.</p>
      <p>Nejlevnější ceny jsou dostupné na leden až březen příštího roku. Zvláště konec března&nbsp;považujeme za ideální období k návštěvě tohoto španělskému města.</br></br></br></br></p>
      </div>
      <div style="background-color: #E6EEFF; padding: 15px;margin: 10px;">
      <ul>
      <li><strong>Rezervace:</strong> přímo na webu <a href="http://www.zaletsi.cz/go-vueling"><strong>Vuelingu</strong></a></li>
      <li><strong>Zavazadlo:&nbsp;</strong>v ceně příruční zavazadlo o rozměrech 55 x 40 x 20 cm a hmotnosti 10 kg</li>
      <li><strong>Délka trvání akce:</strong> do 30.9. nebo vyprodání</li>
      <li><strong>Příklady termínů:</strong>
      <ul>
      <li>19. &#8211; 21.1.</li>
      <li>28.1. &#8211; 1.2.</li>
      <li>13. &#8211; 16.2.</li>
      <li>25. &#8211; 28./29.3. (2197 Kč)</li>
      <li>27. &#8211; 29.3. (2197 Kč)</li>
      </ul>
      </li>
      </ul>
      </div>
      <div class="text_content">
      <h2 style="margin: 15px; text-align: justify;">Ubytování</h2>
      <p> Nejlevnější opravdu hezké ubytování naleznete<br />
       <a href="http://www.booking.com/hotel/es/felipe-2.cs.html?aid=344738;sid=88a15c447c0d0c267f55fbabb4db6ae0;all_sr_blocks=9235601_88154446_0_0_0;checkin=2017-03-25;checkout=2017-03-28;dest_id=-372490;dest_type=city;dist=0;group_adults=2;highlighted_blocks=9235601_88154446_0_0_0;nflt=review_score%3D70%3Bdi%3D2287;room1=A%2CA;sb_price_type=total;srfid=9d8cd7d9888f8e19c1fd582f2857f8d7494ef1a0X7;type=total;ucfs=1&amp;"><strong>v hostelu nedaleko od centra</strong></a>, který ale nabízí moc hezké dloulůžkové pokoje s vlastní nebo sdílenou koupelnou. Cena začíná už okolo 500 Kč na os. a noc, což je v centru Barcelony opravdu velmi dobré.</p>
      <p>Za 900 Kč na os. a noc potom můžete sehnat ubytování v moc <a href="http://www.booking.com/hotel/es/hotelserhsdelport.cs.html?aid=344738;sid=88a15c447c0d0c267f55fbabb4db6ae0;all_sr_blocks=9181701_88191853_0_2_0;checkin=2017-03-25;checkout=2017-03-28;dest_id=-372490;dest_type=city;dist=0;group_adults=2;highlighted_blocks=9181701_88191853_0_2_0;map=1;nflt=review_score%3D70%3Bdi%3D2287%3Bclass%3D3;room1=A%2CA;sb_price_type=total;srfid=9d8cd7d9888f8e19c1fd582f2857f8d7494ef1a0X1;type=total;ucfs=1&amp;#map_closed"><strong>hezkém 3* hotelu</strong></a> vzdáleném pouze 5 minut chůze od Las Ramblas.</p>
      <p>&nbsp;</br></br></br></br>
      </div>
      <div style="text-align: center;">
       <img src="https://my.incomaker.com/content/files/77a02295-6ffa-4bdb-bf67-a18c3ff22d05" alt="Vueling.jpg" data-inco-image-uuid="77a02295-6ffa-4bdb-bf67-a18c3ff22d05" />
      </div>
      ]]></content:encoded>
            <wfw:commentRss>http://zaletsi.cz/primy-let-do-barcelony-za-2061-kc/feed</wfw:commentRss>
          <slash:comments>0</slash:comments>
          </item>
          <item>
          <title>Miami ze Stockholmu &#8211; 6628 Kč</title>
          <link>http://zaletsi.cz/miami-ze-stockholmu-6628-kc</link>
          <comments>http://zaletsi.cz/miami-ze-stockholmu-6628-kc#respond</comments>
          <pubDate>Tue, 27 Sep 2016 09:20:10 +0000</pubDate>
          <dc:creator><![CDATA[Jirka Spolek - www.zaletsi.cz]]></dc:creator>
              <category><![CDATA[Do světa]]></category>

          <guid isPermaLink="false">http://zaletsi.cz/?p=15256</guid>
          <description><![CDATA[Vyrazte během zimy na Floridu, na koupačku v Atlantiku to bude, jen to nebude takové&#8230; Číst více]]></description>
              <content:encoded><![CDATA[<div class="text_content">
      <p style="margin: 15px; text-align: justify;">Vyrazte během zimy na Floridu, na koupačku v Atlantiku to bude, jen to nebude takové kafe, jako během léta. Zpáteční letenku ze Stockholmu do Miami pořídíte za 6628 Kč, například s odletem 4. února a návratem 13. února <strong><u><a href="http://zaletsi.gol.idc.cz/index.php?action=vFlights&amp;flights[0][departureDate]=2017-02-05&amp;flights[0][destination]=MIA&amp;flights[0][origin]=STO&amp;flights[0][departurePlusMinusDay]=3&amp;flights[1][departureDate]=2017-02-14&amp;flights[1][destination]=STO&amp;flights[1][origin]=MIA&amp;flights[1][departurePlusMinusDay]=3&amp;travelers[0]=ADT&amp;returnTicket=on&amp;vendor=SK&amp;dateVariants=close&amp;step=ChooseFromFour">Rezervuj tenhle termín &gt;&gt;</a></u></strong></p>
      </div>
      <div class="text_content">
      <ul>
      <li style="margin: 15px; text-align: justify;"><strong>Rezervace:</strong> <strong><u><a href="http://letenky.zaletsi.cz">v tomto vyhledávači</a></u></strong></li>
      <li style="margin: 15px; text-align: justify;"><strong>Dostupné termíny:</strong> letenky jsou k dispozici v období 16. 1. &#8211; 24. 6. 2017, letenky je třeba uhradit <span style="color: #ff0000;"><strong>nejpozději dnes 27. 9. 2016</strong></span></li>
      <li style="margin: 15px; text-align: justify;"><strong>Zavazadla v ceně: </strong>23kg k odbavení</li>
      </ul>
      </div>
      <div class="text_content">
      <h2 style="margin: 15px; text-align: justify;">Vyplatí se to ze Stockholmu?</h2>
      <p style="margin: 15px; text-align: justify;">Pokud vám nevadí strávit delší čas na cestě a užít si třeba i Stockholm, letenky z Prahy do Stockholmu s <strong><u><a href="http://norwegian.no">Norwegianem</a></u></strong> pořídíte okolo 2000 Kč za zpátečku, dohromady jsme na cca 8600 Kč za zpáteční letenku z Prahy. <strong><u><a href="http://zaletsi.gol.idc.cz/index.php?action=vFlights&amp;flights[0][departureDate]=2017-02-05&amp;flights[0][destination]=MIA&amp;flights[0][origin]=PRG&amp;flights[0][departurePlusMinusDay]=3&amp;flights[1][departureDate]=2017-02-14&amp;flights[1][destination]=PRG&amp;flights[1][origin]=MIA&amp;flights[1][departurePlusMinusDay]=3&amp;travelers[0]=ADT&amp;returnTicket=on&amp;dateVariants=close&amp;step=ChooseFromFour">Běžná letenka</a></u></strong> z Prahy začíná na 13000 Kč s British Airways.</p>
      <h2 style="margin: 15px; text-align: justify;">Ubytování na Floridě</h2>
      <p style="margin: 15px; text-align: justify;">Na pláže se nebojte dojet autem, 5 minut jízdy od pláže vychází v klidnější lokalitě Fort Lauderdale <strong><u><a href="http://www.booking.com/hotel/us/bali-hai-motel-fort-lauderdale.cs.html?aid=344738;sid=fe0b521b82f999b947536c56b60987e7;checkin=2017-02-04;checkout=2017-02-13;ucfs=1;highlighted_blocks=74378002_90386288_2_0_0;all_sr_blocks=74378002_90386288_2_0_0;room1=A,A;dest_type=city;dest_id=20022339;srfid=08eda31b9a938afec736af5ef22c2fed21592723X6;highlight_room=">tento hotel.<br />
      </a></u></strong></p>
      <p style="margin: 15px; text-align: justify;"><span style="line-height: 1.6;">Přímo na South Beach pak vychází ještě rozumně </span><u style="line-height: 1.6;"><strong><a href="http://www.booking.com/hotel/us/sapphire-south-beach.cs.html?aid=344738;sid=fe0b521b82f999b947536c56b60987e7;checkin=2017-02-04;checkout=2017-02-13;ucfs=1;highlighted_blocks=135892701_87517573_2_0_0;all_sr_blocks=135892701_87517573_2_0_0;room1=A,A;dest_type=city;dest_id=20023182;srfid=bc1facc4deda6dd411fcc4b82937567cef06c1f1X2;highlight_room=">tohle studio.</a></strong></u><strong> </strong><span style="line-height: 1.6;">Nicméně pozor na parking, na South Beach je to velmi problematické a půjčené auto budete muset nechat v garážích.</span></p>
      <h2 style="margin: 15px; text-align: justify;"><span style="line-height: 1.2;">Půjčení auta</span></h2>
      <p style="margin: 15px; text-align: justify;">Na samotnou Soutch Beach se sice dostanete autobusem z letiště, ale jestli chcete jet na Keys až dolů na Key West, do národního parku Everglades na vzášedla a aligátory, bez auta se neobejdete. Mrkněte do <strong><span style="text-decoration: underline;"><a href="http://www.zaletsi.cz/autopujcovna" target="_blank">naší autopůjčovny &gt;&gt;</a></span></strong></p>
      <p style="margin: 15px; text-align: justify;">Půjčení auta na Floridě funguje tak, že si ho na letišti vyzvednete, dokonce si vyberete z řady zaparkovaných vozů, které si chcete půjčit a pak ho tam před odletem vrátíte.</p>
      <h2 style="margin: 15px; text-align: justify;">ESTA</h2>
      <p style="margin: 15px; text-align: justify;">Nezapomeňte, že pro vstup do USA potřebujete elektronické potvrzení ESTA za 14 USD, návod, jak na něj a co je potřeba před žádostí si připravit <strong><span style="text-decoration: underline;"><a href="http://www.zaletsi.cz/esta" target="_blank">najdete u nás.</a></span></strong></p>
      <p style="margin: 15px; text-align: justify;"><strong>Náhled letenky za 6628 Kč ze Stockholmu</strong></p>
      <p style="margin: 15px; text-align: justify;"><a href="http://zaletsi.gol.idc.cz/index.php?action=vFlights&amp;flights[0][departureDate]=2017-02-05&amp;flights[0][destination]=MIA&amp;flights[0][origin]=STO&amp;flights[0][departurePlusMinusDay]=3&amp;flights[1][departureDate]=2017-02-14&amp;flights[1][destination]=STO&amp;flights[1][origin]=MIA&amp;flights[1][departurePlusMinusDay]=3&amp;travelers[0]=ADT&amp;returnTicket=on&amp;vendor=SK&amp;dateVariants=close&amp;step=ChooseFromFour" target="_blank" rel="attachment wp-att-15259"><img class="aligncenter wp-image-15259" src="http://zaletsi.cz/wp-content/uploads/2016/09/6628.jpg" alt="6628" width="853" height="501" srcset="http://zaletsi.cz/wp-content/uploads/2016/09/6628-300x176.jpg 300w, http://zaletsi.cz/wp-content/uploads/2016/09/6628-768x450.jpg 768w, http://zaletsi.cz/wp-content/uploads/2016/09/6628-1024x601.jpg 1024w, http://zaletsi.cz/wp-content/uploads/2016/09/6628.jpg 1110w" sizes="(max-width: 853px) 100vw, 853px" /></a></p>
      </div>
      ]]></content:encoded>
            <wfw:commentRss>http://zaletsi.cz/miami-ze-stockholmu-6628-kc/feed</wfw:commentRss>
          <slash:comments>0</slash:comments>
          </item>
        </channel>
      </rss>`,
  "text/xml")
};
