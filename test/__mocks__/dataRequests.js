import {channels as defaultChannels, offers as defaultOffers} from '../__data__/channelsOffers';

export function fetchChannels() {
  return {
    then: jest.fn(callback => {
      callback(mockChannels);
      return {
        catch: jest.fn()
      };
    })
  };
}

export function fetchOffers(channelId) {
  return {
    then: jest.fn(callback => {
      callback(mockOffers[channelId]);
      return {
        catch: jest.fn()
      };
    })
  };
}

let mockChannels = defaultChannels;

export function __setMockChannels(channels) {
  mockChannels = channels;
}

export function __resetMockChannels() {
  __setMockChannels(defaultChannels);
}

let mockOffers = defaultOffers;

export function __setMockOffer(channel, offers) {
  mockOffers[channel] = offers;
}

export function __resetMockChannels() {
  mockOffers = defaultOffers;
}